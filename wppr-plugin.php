<?php
/*
Plugin Name: WordPress Product Reviews Plugin
Description: Plugin for easily implementing product reviews into your website.
Version:     0.1
Author:      Maikel Vuister
Author URI:  http://maikelvuister.nl
*/

define( 'WP_DEBUG', true );

if ( ! defined( 'ABSPATH' ) ){
  exit;
}

// functie worden toegevoegd aan zogenaamde hooks,
// eerste argument is de hook
// tweede argument de naam van de functie
// optionele derde en vierde argument geven priority aan
add_action( 'init', 'WPPRP_create_post_type' );
add_action( 'admin_init', 'WPPRP_create_meta_boxes' );
add_action( 'save_post', 'add_WPPRP_review_fields', 10, 2 );
add_action( 'wp_enqueue_scripts', 'wppr_register_style' );

if(! function_exists('wppr_register_style')){

  function wppr_register_style(){
    // functie registreerd de verschillende css bestanden en voert deze uit
    wp_register_style( 'wppr-plugin-font-awesome', plugins_url( 'wppr_plugin/css/font-awesome.css' ) );
    wp_enqueue_style( 'wppr-plugin-font-awesome' );
    wp_register_style( 'wppr-plugin-bootstrap', plugins_url( 'wppr_plugin/css/bootstrap.min.css' ) );
    wp_enqueue_style( 'wppr-plugin-bootstrap' );
  }

}

include_once("admin/admin-panel.php"); // toevoegen admin panel
include_once("shortcodes/review1.php"); //toevoegen shortcode bestand

if(defined('WP_UNINSTALL_PLUGIN') ){
  // TODO:
  //delete options, tables or anything else
}

?>
