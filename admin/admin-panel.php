<?php
if ( ! defined( 'ABSPATH' ) ){
  exit;
}

if(is_admin()){

  if(! function_exists('WPPRP_create_post_type')){

    function WPPRP_create_post_type() {
      //functie maakt een extra post type aan
      register_post_type( 'WPPRP_reviews',
        array(
        'labels' => array(
          'name' => 'WPPRP reviews',
          'singular_name' => 'WPPRP review',
          'add_new' => 'Add New',
          'add_new_item' => 'Add New WPPRP review',
          'edit' => 'Edit',
          'edit_item' => 'Edit WPPRP review',
          'new_item' => 'New WPPRP review',
          'view' => 'View',
          'view_item' => 'View WPPRP review',
          'search_items' => 'Search WPPRP reviews',
          'not_found' => 'No WPPRP reviews found',
          'not_found_in_trash' => 'No WPPRP reviews found in Trash',
          'parent' => 'Parent WPPRP reviews'
        ),

          'public' => true,
          'menu_position' => 15,
          'supports' => array( 'title',
                               'editor',
                               'thumbnail',
                             ),
          'taxonomies' => array( '' ),
          'menu_icon' => plugins_url( '../images/menu_icon.png', __FILE__ ),
          'has_archive' => true
        )
      );
    }

  }

  if(! function_exists('WPPRP_create_meta_boxes')){

    function WPPRP_create_meta_boxes() {
      add_meta_box(
        'WPPRP_review_meta_box',
        'WPPRP Review Details',
        'display_WPPRP_review_meta_box',
        'WPPRP_reviews',
        'normal',
        'high'
      );
    }

  }

  if(! function_exists('display_WPPRP_review_meta_box')){

    function display_WPPRP_review_meta_box( $WPPRP_review ) {
        // functie geeft de velden weer in het admin panel

        // Retrieve review data from custom post
        $WPPRP_stars = esc_html( get_post_meta( $WPPRP_review->ID, 'WPPRP_stars', true ) );
        $WPPRP_usability = esc_html( get_post_meta( $WPPRP_review->ID, 'WPPRP_usability', true ) );
        $WPPRP_pricing = esc_html( get_post_meta( $WPPRP_review->ID, 'WPPRP_pricing', true ) );
        $WPPRP_quality = esc_html( get_post_meta( $WPPRP_review->ID, 'WPPRP_quality', true ) );
        $WPPRP_product_link = esc_html( get_post_meta( $WPPRP_review->ID, 'WPPRP_product_link', true ) );
        $WPPRP_reviewer_name = esc_html( get_post_meta( $WPPRP_review->ID, 'WPPRP_reviewer_name', true ) );
        ?>
        <table>
          <tr>
            <td>Rate the product with 1 to 5 stars</td>
            <td><input type="number" min="1" max="5" step=".5" name="WPPRP_stars" value="<?php echo $WPPRP_stars; ?>" /></td>
          </tr>
          <tr>
            <td>Rate the usability of the product with a number from 1 to 5</td>
            <td><input type="number" min="1" max="100" step="1" name="WPPRP_usability" value="<?php echo $WPPRP_usability; ?>" /></td>
          </tr>
          <tr>
            <td>Rate the pricing of the product with a number from 1 to 5</td>
            <td><input type="number" min="1" max="100" step="1" name="WPPRP_pricing" value="<?php echo $WPPRP_pricing; ?>" /></td>
          </tr>
          <tr>
            <td>Rate the quality of the product with a number from 1 to 5</td>
            <td><input type="number" min="1" max="100" step="1" name="WPPRP_quality" value="<?php echo $WPPRP_quality; ?>" /></td>
          </tr>
          <tr>
            <td>Product link</td>
            <td><input type="text" size="60" name="WPPRP_product_link" value="<?php echo $WPPRP_product_link; ?>" /></td>
          </tr>
          <tr>
            <td>Reviewer name</td>
            <td><input type="text" size="60" name="WPPRP_reviewer_name" value="<?php echo $WPPRP_reviewer_name; ?>" /></td>
          </tr>
        </table>
        <?php
    }

  }

  if(! function_exists('add_WPPRP_review_fields')){

    function add_WPPRP_review_fields( $WPPRP_review_id, $WPPRP_review ) {
      // Functie zorgt voor het opslaan van de gevulde velden in het admin panel

      // Check post type for WPPRP reviews
      if ( $WPPRP_review->post_type == 'wpprp_reviews' || $WPPRP_review->post_type == 'WPPRP_reviews' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['WPPRP_stars'] ) && $_POST['WPPRP_stars'] != '' ) {
          update_post_meta( $WPPRP_review_id, 'WPPRP_stars', $_POST['WPPRP_stars'] );
        }
        if ( isset( $_POST['WPPRP_usability'] ) && $_POST['WPPRP_usability'] != '' ) {
          update_post_meta( $WPPRP_review_id, 'WPPRP_usability', $_POST['WPPRP_usability'] );
        }
        if ( isset( $_POST['WPPRP_pricing'] ) && $_POST['WPPRP_pricing'] != '' ) {
          update_post_meta( $WPPRP_review_id, 'WPPRP_pricing', $_POST['WPPRP_pricing'] );
        }
        if ( isset( $_POST['WPPRP_quality'] ) && $_POST['WPPRP_quality'] != '' ) {
          update_post_meta( $WPPRP_review_id, 'WPPRP_quality', $_POST['WPPRP_quality'] );
        }
        if ( isset( $_POST['WPPRP_product_link'] ) && $_POST['WPPRP_product_link'] != '' ) {
          update_post_meta( $WPPRP_review_id, 'WPPRP_product_link', $_POST['WPPRP_product_link'] );
        }
        if ( isset( $_POST['WPPRP_reviewer_name'] ) && $_POST['WPPRP_reviewer_name'] != '' ) {
          update_post_meta( $WPPRP_review_id, 'WPPRP_reviewer_name', $_POST['WPPRP_reviewer_name'] );
        }
      }
    }

  }
}

?>
