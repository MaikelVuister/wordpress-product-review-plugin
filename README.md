wppr_plugin

Plugin Name: WordPress Product Reviews Plugin
Description: Plugin for easily implementing product reviews into your website.

shortcode format => [wpprp_review review="NameOfReview"]

<!-- TODO self -->
verdiepen in:
- esc_url
- esc_url_raw
- wp_filter_kses
- esc_attr
- esc_html
- esc_textarea
- nonce

<!-- TODO -->
Core Functionalities
v Enter product name;
v Enter product description;
v Upload product image;
v Product image needs to be clickable with affiliate link;
v Choose number of stars;
v Minimum is 1, maximum is 5, increments of 0,5;
v Displays the stars as stars and as text “3.5 out of 5 stars”;
- Choose rating of aspect, resulting in a percentage bar;
- Make it possible for user to enter name of a bar;
- Give grade to the aspect-bar;
v Minimum is 1, maximum is 100, increments of 1;
v Select button to send user to different page;
- Settings of the plugin: set standard text, each individual review CAN override;
- Fatsoenlijke opmaak

Technical
v Affiliate link is set once: image and button get filled automatically with the affiliate link;
- Necessary for Schema.or Review Type;
v Product name;
v Name of review;
v Image link;
- Rating value;
v Name of reviewer;
- Organization name / aka website name;
v Date published;
v reviewBody / aka description

Extra Functionalities
- Pro’s and cons
- Choose colors of the bars;
- Make extra text left of the ‘Buy’ Button;
- Show price with Amazon API;
v JSON LD support
- Google Rich snippets
- vertalingen
