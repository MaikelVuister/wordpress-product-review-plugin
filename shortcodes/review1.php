<?php

if ( ! defined( 'ABSPATH' ) ){
  exit;
}

// voegt de shortcode 'wpprp_review' toe en die voert de functie wpprp_review_shortcode uit
add_shortcode('wpprp_review', 'wpprp_review_shortcode');
add_action( 'wp_enqueue_scripts', 'wpprp_review_style_review1' );

if(! function_exists('wpprp_review_style_review1')){

  function wpprp_review_style_review1(){
    wp_register_style( 'wppr-plugin-review1', plugins_url( 'wppr_plugin/shortcodes/css/review1.css' ) );
    wp_enqueue_style( 'wppr-plugin-review1' );
  }
}

if(! function_exists('wpprp_review_shortcode')){

  function wpprp_review_shortcode($atts){
    $post_title = $atts["review"];

    $post = get_page_by_title($post_title, OBJECT, 'wpprp_reviews');
    //mogelijk moet dit zijn WPPRP_reviews vandaar de extra if

    // kijken of de post kan worden gevonden, anders een melding hiervan
    if($post == NULL){
      $post = get_page_by_title($post_title, OBJECT, 'WPPRP_reviews');
      if($post == NULL){
        echo("The review could not be found");
      }
    }
    if($post != NULL){
      // wanneer de post is gevonden data ophalen en in html stoppen
      $WPPRP_stars = esc_html( get_post_meta( $post->ID, 'WPPRP_stars', true ) );
      $WPPRP_usability = esc_html( get_post_meta( $post->ID, 'WPPRP_usability', true ) );
      $WPPRP_pricing = esc_html( get_post_meta( $post->ID, 'WPPRP_pricing', true ) );
      $WPPRP_quality = esc_html( get_post_meta( $post->ID, 'WPPRP_quality', true ) );
      $WPPRP_product_link = esc_html( get_post_meta( $post->ID, 'WPPRP_product_link', true ) );
      $WPPRP_reviewer_name = esc_html( get_post_meta( $post->ID, 'WPPRP_reviewer_name', true ) );

      ?>

      <script type="application/ld+json">
      {
        "@context": "http://schema.org/",
        "@type": "Review",
        "itemReviewed": {
          "@type": "Thing",
          "name": "<?php echo($post_title); ?>"
        },
        "author": {
          "@type": "Person",
          "name": "<?php echo($WPPRP_reviewer_name); ?>"
        },
        "reviewRating": {
          "@type": "Rating",
          "ratingValue": "<?php echo($WPPRP_stars); ?>",
          "bestRating": "5"
        }
      }
      </script>

      <div class="container .wpprp-shortcode-container">
        <div class="row">
          <div class="col col-sm-6 wpprp-img-container">
            <a class="wpprp-link" href = "<?php echo($WPPRP_product_link); ?>">
              <?php echo(get_the_post_thumbnail($post->ID)); ?>
            </a>
          </div>
          <div class="col col-sm-6 wpprp-rating-container">
            <div class="row">
              <div class="col col-sm-12 title">
                <h2><?php echo($post->post_title); ?></h2>
              </div>
            </div> <!-- end of row -->
            <div class="row">
              <div class="col col-sm-12 wpprp-description">
                <?php echo($post->post_content); ?>
              </div>
            </div> <!-- end of row -->
            <div class="row">
              <div class="col col-sm-2 wpprp-usability">
                <h3>Usability</h3>
              </div>
              <div class="col col-sm-8 wpprp-usability">
                <div class="progress">
                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo($WPPRP_usability); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo($WPPRP_usability); ?>%">
                  </div>
                </div>
              </div>
              <div class="col col-sm-2 wpprp-usability">
                <h4><?php echo($WPPRP_usability/10); ?></h4>
              </div>
            </div> <!-- end of row -->
            <div class="row">
              <div class="col col-sm-2 wpprp-price">
                <h3>Pricing</h3>
              </div>
              <div class="col col-sm-8 wpprp-price">
                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo($WPPRP_pricing); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo($WPPRP_pricing); ?>%">
                  </div>
                </div>
              </div>
              <div class="col col-sm-2 wpprp-price">
                <h4><?php echo($WPPRP_pricing/10); ?></h4>
              </div>
            </div> <!-- end of row -->
            <div class="row">
              <div class="col col-sm-2 wpprp-quality">
                <h3>Quality</h3>
              </div>
              <div class="col col-sm-8 wpprp-quality">
                <div class="progress">
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo($WPPRP_quality); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo($WPPRP_quality); ?>%">
                  </div>
                </div>
              </div>
              <div class="col col-sm-2 wpprp-quality">
                <h4><?php echo($WPPRP_quality/10); ?></h4>
              </div>
            </div> <!-- end of row -->
          </div>
        </div> <!-- end of row -->
        <div class="row">
          <div class="col col-sm-6 wpprp-stars-container">
            <?php
              echo(($WPPRP_stars)); ?> out of 5 stars &nbsp<?php
              //todo show amount of stars
              for($i; $i < floor($WPPRP_stars); $i++){
                ?><i class="fa fa-star" aria-hidden="true"></i><?php
              }
              for($j; $j < $WPPRP_stars-$i; $j++){
                ?><i class="fa fa-star-half-o" aria-hidden="true"></i><?php
              }
              for($k; $k < 5 - $i - $j; $k++){
                ?><i class="fa fa-star-o" aria-hidden="true"></i><?php
              }
            ?>
          </div>
          <div class="col col-sm-6 wpprp-link-container">
            <div class="col col-sm-6 wpprp-link-container">
              Add to Amazon Shopping Cart =>   <!-- omzetten naar custom tekst -->
            </div>
            <a class="wpprp-link" href = "<?php echo($WPPRP_product_link); ?>"><div  class="col col-sm-6 wpprp-amazon-button">
              Buy on Amazon <!-- omzetten naar custom tekst -->
            </div></a>
          </div>
        </div> <!-- end of row -->
        <?php if(!$WPPRP_reviewer_name == ""){
          ?>
            <div class="row">
              <div class="col col-sm-12 wpprp-reviewer-container">
                Reviewed by: <?php echo($WPPRP_reviewer_name); ?> | Posted on <?php echo(substr($post->post_date, 0, 10)); ?>
              </div>
            </div> <!-- end of row -->
          <?php
        }else{
          echo("Posted on " . substr($post->post_date, 0, 10));
        }?>
      </div> <!-- end of container -->
      <?php
    }
  }

}
?>
